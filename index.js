#!/usr/bin/nodejs
const fs = require('fs');
const path = require('path');
const ffmpeg = require('fluent-ffmpeg');
const rimraf = require('rimraf');

const torrentsDir = process.env.SNIGGLE_TORRENTS;
const mediaDir = process.env.SNIGGLE_MEDIA;

if (!torrentsDir || !mediaDir) {
  throw new Error('missing env vars SNIGGLE_TORRENTS or SNIGGLE_MEDIA');
}

const logError = err => err ? console.error(err) : void 0;

function toMp4(input, output, cb) {
  ffmpeg(input)
    .on('error', err => cb(err))
    .on('end', () => cb(null))
    .format('mp4')
    .videoCodec('copy')
    .audioCodec('copy')
    .output(output)
    .run();
}



function readTorrent(fp, currTorrent = null) {

  let torrent;
  if (currTorrent) {
    torrent = currTorrent;
  } else {
    torrent = {
      videos: [],
      subs: []
    };
  }

  if (fs.statSync(fp).isDirectory()) {
    const files = fs.readdirSync(fp);
    const filepaths = files.map(f => path.join(fp, f));
    for (let i = 0; i < filepaths.length; i++) {
      torrent = readTorrent(filepaths[i], torrent);
    }
    return torrent;
  }

  const fn = path.basename(fp);
  if (path.extname(fn) === '.mp4' || path.extname(fn) === '.mkv') {
    torrent.videos.push(fp);
  }

  else if (path.extname(fn) === '.srt') {
    torrent.subs.push(fp);
  }

  return torrent;
}

function processTorrent(torrent) {
  if (torrent.videos.length === 0) {
    return;
  }

  /*
   * Pick the biggest video
   */
  let vidmain;
  let topsize = 0;
  torrent.videos.forEach(vid => {
    const stat = fs.statSync(vid);
    if (stat.size >= topsize) {
      topsize = stat.size;
      vidmain = vid;
    }
  });

  /*
   * Pick a sub with 'eng' in it (or the only sub it there is 1)
   */
  let subeng;
  if (torrent.subs.length === 1) {
    subeng = torrent.subs[0];
  }

  else {
    torrent.subs.forEach(sub => {
      if (sub.toLowerCase().includes('eng')) {
        subeng = sub;
      }
    });
  }

  const fn = path.basename(vidmain);
  let basename;
  if (path.extname(fn) === '.mp4') {
    basename = path.basename(fn, '.mp4');
    fs.rename(vidmain, path.join(mediaDir, fn), logError);
  }

  else if (path.extname(fn) === '.mkv') {
    basename = path.basename(fn, '.mkv');
    const mp4name = basename + '.mp4';
    toMp4(vidmain, path.join(mediaDir, mp4name), logError);
  }

  else {
    throw new Error('UNREACHABLE');
  }

  if (subeng) {
    fs.rename(subeng, path.join(mediaDir, basename + '.srt'), logError);
  }
}

const filepaths = fs.readdirSync(torrentsDir).map(f => path.join(torrentsDir, f));
const torrents = [];
for (let i = 0; i < filepaths.length; i++) {
  torrents[i] = readTorrent(filepaths[i]);
}

torrents.forEach(processTorrent);

process.on('beforeExit', function exitHandler() {
  if (process.argv[2] === '--delete') {
    filepaths.forEach(rimraf.sync);
  }
});
