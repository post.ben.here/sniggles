#!/bin/bash -e

rm -r test

mkdir test
mkdir test/media
mkdir test/torrents
mkdir test/torrents/almonds.are.goodS01E02
mkdir test/torrents/almonds.are.goodS01E02/subs

echo "really good" > test/torrents/almonds.are.goodS01E02/almonds.are.goodS01E02.mp4
echo "really good" > test/torrents/almonds.are.goodS01E02/subs/1_eng.srt
echo "really good" > test/torrents/almonds.are.goodS01E02/subs/1_spn.srt

SNIGGLE_TORRENTS="./test/torrents" SNIGGLE_MEDIA="./test/media" ./index.js $1
